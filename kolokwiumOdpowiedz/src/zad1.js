const _ = require("lodash")

class Library{
    constructor(){
        this.books=[]
    }
    addBook(title,author){
        const nowyobj={title:title,author:author}
        this.books.push(nowyobj)

    }
    findBookByAuthor(author){
        const wynik=this.books.filter(el=>el.author===author)
        return wynik;
    }
    sortBookByTitle(){
        const wynik=this.books.sort((a,b)=>a.title.localeCompare(b.title))
        this.books=wynik
    }
    removeBook(title){
        const wynik=this.books.filter(el=>el.title===title)
        const wynik2=_.difference(this.books,wynik)
        this.books=wynik2

    }
    display(){
        console.log(this.books)
    }
}

//dodałem metodę display aby latwiej sprawdzic dzialanie innych metod

const myLib=new Library
myLib.addBook("Harry Potter","J.K. Rowling")
console.log(myLib.findBookByAuthor("J.K. Rowling"))
myLib.addBook("Lord Of the rings","J.R.R. Tolkien")
myLib.addBook("Harry Potter","J.K. Rowling")
myLib.display()
myLib.sortBookByTitle()
myLib.display()
myLib.removeBook("Harry Potter")
myLib.display()





