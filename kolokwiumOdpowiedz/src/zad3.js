const _=require("lodash")
function mergeObjects(obj1,obj2) {
    const wynik=_.merge(obj1,obj2)
    return wynik

    
}
const a={person:{name:"John",age:30}}
const b={person:{age:35,city:"New York"}}
console.log(mergeObjects(a,b))

const x={}
const y={name:"Alice",age:25}
console.log(mergeObjects(x,y))

const o={person:{name:"John",address:{city:"New York",street:"Broadway"}}}
const p={person:{age:35}}
console.log(mergeObjects(o,p))
